Mess Tools
==========

A collection of useful utilities to simplify doing things The Mess Way. To install Mess Tools you need Homebrew. Install Homebrew and you can install Mess Tools thusly:

    $ brew install https://bitbucket.org/thisismess/mess-tools/raw/master/formulae/messtools.rb

And update Mess Tools by running:

    $ brew upgrade https://bitbucket.org/thisismess/mess-tools/raw/master/formulae/messtools.rb

Reference
=========

sync-trust
--------------
Copy your public SSH key to another host. You must have already generated a SSH identity before running this command. After running ```sync-trust``` you should be able to shell into the remote host without being prompted for a password.

sandbox-exec-no-network
--------------
Launch a sandboxed instance of an app with network access disallowed. The sandbox settings only apply to that specific instance and will not permanently affect the app.

json-format
--------------
Reads JSON input from standard input and writes pretty-print formatted JSON to standard output.

build-universal
--------------
Build a universal framework for an Xcode project.

mess-django-init
--------------
Initialize and configure a Django project.

Contributing to Mess Tools
==========================

Mess Tools is intended to be a general purpose resource for making common tasks easier or faster. If you have built a tool that fits this description, feel free to add it to the repository.

Naming your tool
----------------
You should take care to name your tool something useful and self-explanatory. For example, ```django-init-project``` is a good name, ```djinitp``` is a terrible name and will get you yelled at.

Where to add your tool
----------------------

If you are adding a self-contained script (i.e., one single file) to the tools, you should add that script to the ```/bin``` directory.

If you are adding a tool that has resources or multiple scripts, you should put the tool's source and resources in their own directory and place that directory under the directory ```/tools```. You should then add a single driver script to ```/bin``` to invoke your tool.

