require 'formula'

class Messtools < Formula
  
  url 'https://jshedd@bitbucket.org/thisismess/mess-tools.git', :using => :git
  version '31'
  
  def install
    bin.install('bin/mess-django-init')
    bin.install('bin/mess-json-format')
    bin.install('bin/mess-sync-trust')
    bin.install('bin/mess-build-universal')
    bin.install('bin/mess-update-tools')
    bin.install('bin/mess-bc-sync-files')
    bin.install('bin/mess-exec-no-network')
    share.install('share/messtools')
  end
  
end